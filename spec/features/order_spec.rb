require 'spec_helper'

feature 'order' do
    scenario 'Order by payment slip' do
        login
        search_item
        click_on_product
        add_to_cart
        checkout
        payment
    end

    def search_item
        find('#suggestion-search').set('smartphone galaxy s7')
        click_button('Procurar')
    end

    def click_on_product
        page.find(:xpath, "//*[@id='product-list']/section/ul/li[8]/section/a/figure/img").click
    end

    def add_to_cart
        click_button('Adicionar ao carrinho')
        click_button('Continuar')
        find('.cart-icon').click
    end

    def checkout
        click_button('Finalizar compra', match: :first)
        click_button('Entregar nesse endereço')
        find('.btn.btn-primary.continue-button', match: :first).click
    end

    def payment
        page.find(:xpath, '/html/body/div[1]/section/div/div/div[6]/div[1]/ul/li[2]/a').click
        sleep 5
        click_button('Finalizar compra')
      end
end
